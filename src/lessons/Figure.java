/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lessons;

/**
 *
 * @author Admin
 */
public abstract class Figure {
    
    public abstract double GetArea();
  
    public abstract String GetName();
       
}
