/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lessons;

/**
 *
 * @author Admin
 */
public class Rectangle extends Figure{

    int _a=0;

    public Rectangle(int a) {
        _a=a;      
    }
       
    @Override
    public double GetArea() {
        return _a * _a;
    }

    @Override
    public String GetName() {
        return "Квадрат";
    }
    
}
