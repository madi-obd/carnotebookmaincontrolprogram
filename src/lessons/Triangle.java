/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lessons;

/**
 *
 * @author Admin
 */
public class Triangle extends Figure{

    int _katetA=0, _katetB=0;

    public Triangle(int katetA, int katetB) {
        
        _katetA=katetA; 
        _katetB=katetB;
    }
       
    @Override
    public double GetArea() {
        return _katetA*_katetB/2;
    }

    @Override
    public String GetName() {
        return "Треугольник";
    }
    
    
    
}
