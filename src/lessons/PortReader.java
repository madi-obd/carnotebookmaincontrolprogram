/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lessons;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import java.awt.Color;
import java.io.BufferedReader;
import java.sql.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import sun.nio.cs.StreamDecoder;

/**
 *
 * @author Admin
 */
public class PortReader {
    
    
    
    public PortReader()
    {
        super();
    }
    
    void connect ( String portName ) throws Exception
    {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if ( portIdentifier.isCurrentlyOwned() )
        {
            System.out.println("Error: Port is currently in use");
        }
        else
        {
            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
            
            if ( commPort instanceof SerialPort )
            {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                
                InputStream in = serialPort.getInputStream();
                OutputStream out = serialPort.getOutputStream();
                
                (new Thread(new SerialReader(in))).start();
               

            }
            else
            {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }     
    }
    
    /** */
    public static class SerialReader implements Runnable 
    {
        InputStream in;
        
        public SerialReader ( InputStream in )
        {
            this.in = in;
        }
        
        public void run ()
        {
            byte[] buffer = new byte[1024];
            Connection conn = null;
            Statement stmt = null;
            int len = -1;
            try
            {
                Class.forName("com.mysql.jdbc.Driver");

                //STEP 3: Open a connection
                System.out.println("Connecting to a selected database...");
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                System.out.println("Connected database successfully...");

                //STEP 4: Execute a query
                System.out.println("Inserting records into the table...");
                stmt = conn.createStatement();
                 len = this.in.read(buffer)    ;   
                 String fromSensor="";
                while ( ( len = this.in.read(buffer)) > -1 )
                {
                    fromSensor += new String(buffer,0,len);
                    System.out.print(fromSensor);
                    
                    try{
                    
                    String sql = "INSERT INTO meteodata " + 
                    "VALUES (NOW(), 10, 10,'"+ fromSensor +"')";
                    stmt.executeUpdate(sql);
                  
                    fromSensor="";
                    }catch(Exception ex)
                    {
                          fromSensor="";
                        System.err.println(ex.getMessage());
                        
                    }
                    
                  
                      //STEP 2: Register JDBC driver
      
      
            
      //String sql2 = "delete  from meteodata";
      //stmt.executeUpdate(sql2);
           
      System.out.println("Inserted records into the table...");

 
   
      //Handle errors for Class.forName
   }
   }
            catch ( Exception e )
            {
                e.printStackTrace();
            }            
        }
    }

    
    
    // JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/meteostation";

   //  Database credentials
   static final String USER = "root";
   static final String PASS = "";
    
   public static void main(String[] args) throws IOException {
   Connection conn = null;
   Statement stmt = null;
  
  
        try
        {
             (new PortReader()).connect("COM18");     
            //(new PortReader()).connect("COM 18");
       
  
     

 
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            conn.close();
      }catch(SQLException se){
      }// do nothing
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }//end finally try
   }//end try
   System.out.println("Goodbye!");
}//end main
}