/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brakelogic;

/**
 *
 * @author Admin
 */
public class BrakeCalculator {
    
    
    
    public static double brakeCalculate(double speed, double distance)
    {
        
        double p00 =-56.53 ;
        double p10 = -1.473;
        double p01 =8.684;
        double p11 =-0.1908;
        double p02 = -0.2968 ;
        double p12=0.00780;
        double p03=0.002243;
        double p13 =-5.752e-05;
        double p04 =1.212e-05;
        double p14 =-8.142e-08;
        double p05 =-1.222e-07;
        
        return p00+p10*speed+p01*distance+p11*speed*distance+p02* Math.pow(distance,2)+p12*speed*Math.pow(distance,2)+p03*Math.pow(distance,3)+p13*speed*Math.pow(distance,3)+p04*Math.pow(distance,4)
                +p14*speed*Math.pow(distance,4)+p05*Math.pow(distance,5);
        
        
        
    }
  
      public static double brakeCalculateLinear(double speed, double distance)
    {
        
        double p00 =-46.11 ;
        double p10 = -0.603;
        double p01 =0.8634;
        
        
        return p00+p10*speed+p01*distance;
        
        
        
    }
//     public static void main(String[] args) {
//         
//         System.out.println( "Brake value= " + brakeCalculate(30, 5));
//     
//     }
    
    
}
