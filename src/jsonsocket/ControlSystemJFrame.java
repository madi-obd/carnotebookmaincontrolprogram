/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonsocket;

import brakelogic.BrakeCalculator;
import java.util.Enumeration;
import canlogic.USBtinLibDemo;
import csv.Csv;
import de.fischl.usbtin.CANMessage;
import de.fischl.usbtin.CANMessageListener;
import de.fischl.usbtin.USBtin;
import de.fischl.usbtin.USBtinException;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import helpers.ByteHelper;
import innertialnavi.CarNav;
import innertialnavi.InnertialNaviServer;
import java.awt.AWTEventMulticaster;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.neuroph.core.NeuralNetwork;




/**
 *
 * @author Мади
 */
public class ControlSystemJFrame extends javax.swing.JFrame  implements CANMessageListener  {

     /** CAN message identifier we look for */
    static final int WATCHID_RPM_SPEED = 0x201;
    
    static final int WATCHID_KPP = 0x3E9;
    
    
    static final int WATCHID_ABS_SPEED = 0x4B0;
    double speedABSInt=0, distToBrakeABS=0, vision_distance=0;   
    int speed=0, RPM=0;
    String speedABS="";
    
    Communicator communicator = null;
    CommunicatorKPP communicatorKPP = null;
    JSONServer js;
    Timer senderTimer;
    Timer loggerTimer;
    Timer neuronTimer;
    int neuronBreake=0;
    
    Csv.Writer experimentLoggerRaw = null;
    Csv.Writer experimentLoggerPrecalcutated = null;
    double distToBrake=0;
    NeuralNetwork neuralNetwork;
    
     @Override
    public void receiveCANMessage(CANMessage canmsg) {

        // In this example we look for CAN messages with given ID
        if (canmsg.getId() == WATCHID_RPM_SPEED) {
            
            // juhuu.. match!
            
            // print out message infos
            System.out.println("Watched message: " + canmsg);
            System.out.println(
                    "  id:" + canmsg.getId()
                    + " dlc:" + canmsg.getData().length
                    + " ext:" + canmsg.isExtended()
                    + " rtr:" + canmsg.isRtr());
          
            // and now print payload
            for (byte b : canmsg.getData()) {
                System.out.print(" " + b);
                RPM=ByteHelper.ByteToUnsignedInt(canmsg.getData()[0])*256+ByteHelper.ByteToUnsignedInt(canmsg.getData()[1]) ;
                speed=(ByteHelper.ByteToUnsignedInt(canmsg.getData()[4])*256+ ByteHelper.ByteToUnsignedInt(canmsg.getData()[5]))/100;
            }
             jTextFieldBrakeActivity.setText(js.getDfa().getAcceleratioRate()+"");          
            this.textFieldCANRPM.setText(Integer.toString(RPM));
            
            this.textFieldCANSpeed.setText(Integer.toString(speed));
            System.out.println();
            js.getDfc().setEngineRPM(RPM);
            js.getDfc().setSpeed(speed);
            
           
        } else if(canmsg.getId() == WATCHID_ABS_SPEED){
           speedABS=ByteHelper.ByteToUnsignedInt(canmsg.getData()[0])+" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[1])+" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])
                   +" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[3])+" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[4])+" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[5])
                   +" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[6])+" "+ByteHelper.ByteToUnsignedInt(canmsg.getData()[7]);
        speedABSInt=((ByteHelper.ByteToUnsignedInt(canmsg.getData()[0])-39)*256+(ByteHelper.ByteToUnsignedInt(canmsg.getData()[1])-16) )/200;
        
        }
        else if(canmsg.getId() == WATCHID_KPP)
        {
            String KPP = "P";
            
            if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==0){
                
                KPP="P";
            }
             if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==16){
                
                KPP="R";
            }
             
              if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==32){
                
                KPP="N";
            }
               if(ByteHelper.ByteToUnsignedInt(canmsg.getData()[2])==48){
                
                KPP="D";
            }
          //  communicatorKPP.changeDriveServer.setCanDriveKPP(KPP);
            
            
            System.out.println("KPP = " +KPP);
        }
        
        
        
        
    }
    
   
    /**
     * Creates new form ControlSystemJFrame
     */
    public ControlSystemJFrame() {
        initComponents();
        createObjects();
        
        js = new JSONServer("10.0.2.2", 45001);
     ///   neuralNetwork = NeuralNetwork.createFromFile("C:\\Users\\Admin\\Documents\\NetBeansProjects\\CarController\\Neural Networks\\NewNeuralNetwork1.nnet");
        
        js.Start();
    }
    
    private void createObjects()
    {
        communicator = new Communicator(this);
        communicator.searchForPorts();  
        communicatorKPP = new CommunicatorKPP(this);
        communicatorKPP.searchForPorts();  
        
        
       Enumeration ports= CommPortIdentifier.getPortIdentifiers();
        while (ports.hasMoreElements())
        {
            CommPortIdentifier curPort = (CommPortIdentifier)ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                this.cboxPorts1.addItem(curPort.getName());                
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textField1 = new java.awt.TextField();
        jLabel5 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        textField2 = new java.awt.TextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        textFieldCANSpeed = new java.awt.TextField();
        textFieldCANRPM = new java.awt.TextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtLog1 = new javax.swing.JTextArea();
        cboxPorts = new javax.swing.JComboBox();
        btnConnect = new javax.swing.JButton();
        btnDisconnect = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnDisconnect1 = new javax.swing.JButton();
        cboxPorts1 = new javax.swing.JComboBox();
        btnConnect1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        jTextFieldDistToPoint = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldVisionDist = new javax.swing.JTextField();
        jTextFieldBrakeActivity = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jCheckBoxAutoBrakeAnable = new javax.swing.JCheckBox();
        jToggleButtonAutobrake = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setText("Руль");

        jLabel2.setText("Педали");

        textField1.setText("textField1");

        jLabel5.setText("От Андроид");

        jButton2.setText("Стартовать сервер");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        textField2.setText("textField2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel1)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton2))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(textField1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(textField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel3.setText("Скорость");

        jLabel4.setText("Обороты");

        textFieldCANSpeed.setText("textField1");

        textFieldCANRPM.setText("textField2");

        jLabel6.setText("С шины CAN");

        txtLog.setEditable(false);
        txtLog.setColumns(20);
        txtLog.setLineWrap(true);
        txtLog.setRows(5);
        txtLog.setFocusable(false);
        jScrollPane2.setViewportView(txtLog);

        txtLog1.setEditable(false);
        txtLog1.setColumns(20);
        txtLog1.setLineWrap(true);
        txtLog1.setRows(5);
        txtLog1.setFocusable(false);
        jScrollPane3.setViewportView(txtLog1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textFieldCANSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldCANRPM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(textFieldCANSpeed, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(textFieldCANRPM, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        btnConnect.setText("Connect");
        btnConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConnectActionPerformed(evt);
            }
        });

        btnDisconnect.setText("Disconnect");
        btnDisconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDisconnectActionPerformed(evt);
            }
        });

        jLabel7.setText("Приводы");

        jLabel8.setText("CAN");

        btnDisconnect1.setText("Disconnect");
        btnDisconnect1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDisconnect1ActionPerformed(evt);
            }
        });

        btnConnect1.setText("Connect");
        btnConnect1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConnect1ActionPerformed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(0, 255, 102));

        jLabel9.setText("Запись данных эксперимента");

        jToggleButton1.setText("Запись");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        jToggleButton2.setText("Стоп");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });

        jTextFieldDistToPoint.setText("100");

        jLabel10.setText("Расстояние до точки");

        jLabel11.setText("Расстояние видимости");

        jTextFieldVisionDist.setText("100");

        jTextFieldBrakeActivity.setText("0");

        jLabel13.setText("Тормозная активность");

        jCheckBoxAutoBrakeAnable.setText("Autobrake Enable");

        jToggleButtonAutobrake.setText("go Autobrake");
        jToggleButtonAutobrake.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButtonAutobrakeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jCheckBoxAutoBrakeAnable)
                        .addGap(20, 20, 20))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldBrakeActivity, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jToggleButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jToggleButton2)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(26, 26, 26)
                                .addComponent(jTextFieldDistToPoint, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldVisionDist)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jToggleButtonAutobrake)
                        .addGap(26, 26, 26))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jCheckBoxAutoBrakeAnable))
                .addGap(8, 8, 8)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldDistToPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jToggleButtonAutobrake))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextFieldVisionDist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jTextFieldBrakeActivity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jToggleButton1)
                    .addComponent(jToggleButton2))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cboxPorts1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnConnect1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDisconnect1)))
                        .addGap(238, 238, 238)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cboxPorts, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnConnect)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDisconnect)))))
                .addContainerGap(209, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboxPorts, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnConnect)
                            .addComponent(btnDisconnect)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cboxPorts1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnConnect1)
                            .addComponent(btnDisconnect1))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    USBtin usbtin ;
    
    String canDrive="P";
    
    private void btnConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConnectActionPerformed
             
       
                
        communicator.connect();
        if (communicator.getConnected() == true)
        {
            if (communicator.initIOStream() == true)
            {
                communicator.initListener();
            }
        }
        
       
        senderTimer = new Timer();
        
        senderTimer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                try{
//                    if(communicatorKPP!=null)
//                    {
//                        
//                        communicatorKPP.changeDriveServer.setNewDriveKPP(js.getDfa().getDriveKPP());
//                        communicatorKPP.changeDriveServer.setCanDriveKPP(canDrive);
//                        
//                    }
                    if(communicator!=null){
                        
                        if(!jCheckBoxAutoBrakeAnable.isSelected()){
                        //бизнес логика контроль превышения оборотов
                        if(js.getDfc().engineRPM>2700||js.getDfc().speed>33){
                            if(js.getDfa().getAcceleratioRate()>0){
                                communicator.writeData(0, js.getDfa().getSteeringWheelRate());
                            }
                            else{
                                communicator.writeData(js.getDfa().getAcceleratioRate(), js.getDfa().getSteeringWheelRate());                               
                            }                            
                        }else{
                            communicator.writeData(js.getDfa().getAcceleratioRate(), js.getDfa().getSteeringWheelRate());
                        }
                        }
                        else
                        {
                            if(distToBrake<vision_distance){
                            communicator.writeData(neuronBreake, js.getDfa().getSteeringWheelRate());
                            }
                            else
                            {
                                
                                 if(js.getDfc().engineRPM>2700||js.getDfc().speed>33){
                            if(js.getDfa().getAcceleratioRate()>0){
                                communicator.writeData(0, js.getDfa().getSteeringWheelRate());
                            }
                            else{
                                communicator.writeData(js.getDfa().getAcceleratioRate(), js.getDfa().getSteeringWheelRate());                               
                            }                            
                        }
                                 else{
                            communicator.writeData(js.getDfa().getAcceleratioRate(), js.getDfa().getSteeringWheelRate());
                        }
                            }
                                
                            
                        }
                    }
                }catch(Exception ex)
                {
                    
                    ex.printStackTrace();
                    
                }
            }
        }, 150, 150);
        
        
        
        
        
    }//GEN-LAST:event_btnConnectActionPerformed

    private void btnDisconnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDisconnectActionPerformed
        communicator.disconnect();
        
       
    }//GEN-LAST:event_btnDisconnectActionPerformed

    private void btnDisconnect1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDisconnect1ActionPerformed
        // TODO add your handling code here:
         try {
            usbtin.closeCANChannel();
            usbtin.disconnect();
        } catch (USBtinException ex) {
            Logger.getLogger(ControlSystemJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnDisconnect1ActionPerformed

    private void btnConnect1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConnect1ActionPerformed
        // TODO add your handling code here:
         try {
             
            String selectedPort = (String)this.cboxPorts1.getSelectedItem();
               
            CommPort commPort = null;
            // create the instances
            usbtin = new USBtin();
            
            // connect to USBtin and open CAN channel with 10kBaud in Active-Mode
            usbtin.connect(selectedPort); // Windows e.g. "COM3"
            usbtin.addMessageListener(this);            
            usbtin.openCANChannel(500000, USBtin.OpenMode.ACTIVE);

            // send an example CAN message (standard)
          //  usbtin.send(new CANMessage(0x100, new byte[]{0x11, 0x22, 0x33}));
            // send an example CAN message (extended)
          //  usbtin.send(new CANMessage(0x101, new byte[]{0x44}, true, false));

            // now wait for user input
            System.out.println("Listen for CAN messages (watch id=" + WATCHID_RPM_SPEED + ") ... press ENTER to exit!");
            

            // close the CAN channel and close the connection
            
        } catch (USBtinException ex) {
            
            // Ohh.. something goes wrong while accessing/talking to USBtin           
            System.err.println(ex);            
            
        } 
    }//GEN-LAST:event_btnConnect1ActionPerformed
 long timeStampOld =System.currentTimeMillis();
 InnertialNaviServer inertialNav;
    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        
       
    }//GEN-LAST:event_formWindowClosed

    private void btnConnectKPPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConnectKPPActionPerformed
        // TODO add your handling code here:
         communicatorKPP.connect();
        if (communicatorKPP.getConnected() == true)
        {
            if (communicatorKPP.initIOStream() == true)
            {
                communicatorKPP.initListener();
            }
        }
        communicatorKPP.start();
             
    }//GEN-LAST:event_btnConnectKPPActionPerformed

    private void btnDisconnectKPPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDisconnectKPPActionPerformed
        // TODO add your handling code here:
        communicatorKPP.disconnect();
    }//GEN-LAST:event_btnDisconnectKPPActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void textField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textField2ActionPerformed

    private void jToggleButtonAutobrakeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButtonAutobrakeActionPerformed
        // TODO add your handling code here:

        neuronTimer = new Timer();
        neuronTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                //            distToBrake=(distToBrake-js.getDfc().speed*1000*0.1/3600);
                //
                //            neuralNetwork.setInput(distToBrake, js.getDfc().speed, Integer.parseInt(jTextFieldVisionDist.getText()));
                //          //  neuralNetwork.setInput(0, 0, 0);
                //             // calculate network
                //            neuralNetwork.calculate();
                // get network output
                //             double[] networkOutput = neuralNetwork.getOutput();
                neuronBreake   = (int)BrakeCalculator.brakeCalculate((double)js.getDfc().speed,distToBrake);

            }
        }, 100, 100);
    }//GEN-LAST:event_jToggleButtonAutobrakeActionPerformed

    private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
        // TODO add your handling code here:

        experimentLoggerRaw.close();
        experimentLoggerRaw = null;

        experimentLoggerPrecalcutated.close();
        experimentLoggerPrecalcutated = null;
        loggerTimer.cancel();
        jToggleButton1.setSelected(false);
    }//GEN-LAST:event_jToggleButton2ActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        // TODO add your handling code here:

        distToBrake=Integer.parseInt(jTextFieldDistToPoint.getText());
        vision_distance=Integer.parseInt(jTextFieldVisionDist.getText());
        distToBrakeABS=Integer.parseInt(jTextFieldDistToPoint.getText());
        experimentLoggerRaw = new Csv.Writer(System.currentTimeMillis()+"Raw.csv").delimiter(' ');

        experimentLoggerPrecalcutated = new Csv.Writer(System.currentTimeMillis()+"Precalculated.csv").delimiter(' ');

        loggerTimer = new Timer();

        inertialNav = new InnertialNaviServer(0, 0, 0, 0, 2.639f);

        loggerTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(experimentLoggerRaw!=null){
                    experimentLoggerRaw.value(js.getDfc().speed+"").value(js.getDfa().getAcceleratioRate()+"").
                    value(System.currentTimeMillis()+"").value(jTextFieldDistToPoint.getText()).value(jTextFieldVisionDist.getText()).value(speedABS).newLine();

                    distToBrake = (distToBrake-js.getDfc().speed*1000*0.1/3600);
                    //jTextFieldDistanse.setText(distToBrake+" ");

                    distToBrakeABS =(distToBrakeABS-speedABSInt*1000*0.1/3600);
                    experimentLoggerPrecalcutated.value(jTextFieldVisionDist.getText()).value(js.getDfc().speed+"").value(distToBrake+"").value(js.getDfa().getAcceleratioRate()+"").value(speedABS).value(distToBrake+"").newLine();

                    long thisTime = System.currentTimeMillis();

                    long deltaTime=thisTime-timeStampOld;

                    timeStampOld=thisTime;

                    float delta = deltaTime/1000;

                    inertialNav.CalculateNavigation(js.getDfa().getSteeringWheelRate(), js.getDfc().speed, delta);

                
                }
            }
        }, 100, 100);
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ControlSystemJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ControlSystemJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ControlSystemJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ControlSystemJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ControlSystemJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnConnect;
    public javax.swing.JButton btnConnect1;
    public javax.swing.JButton btnDisconnect;
    public javax.swing.JButton btnDisconnect1;
    public javax.swing.JComboBox cboxPorts;
    public javax.swing.JComboBox cboxPorts1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBoxAutoBrakeAnable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextFieldBrakeActivity;
    private javax.swing.JTextField jTextFieldDistToPoint;
    private javax.swing.JTextField jTextFieldVisionDist;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JToggleButton jToggleButtonAutobrake;
    private java.awt.TextField textField1;
    private java.awt.TextField textField2;
    private java.awt.TextField textFieldCANRPM;
    private java.awt.TextField textFieldCANSpeed;
    public javax.swing.JTextArea txtLog;
    public javax.swing.JTextArea txtLog1;
    // End of variables declaration//GEN-END:variables
}
