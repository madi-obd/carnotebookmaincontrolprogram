/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonsocket;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ChangeDriveServer implements Runnable{
    
    public CommunicatorKPP communicatorKPP=null;
    
    int needSmallGo = 0;
    int SleepTime = 500;
    String canDriveKPP = "P";
    
    String newDriveKPP = "P";

    public String getCanDriveKPP() {
        return canDriveKPP;
    }

    public void setCanDriveKPP(String canDriveKPP) {
        this.canDriveKPP = canDriveKPP;
    }

    public String getNewDriveKPP() {
        return newDriveKPP;
    }

    public void setNewDriveKPP(String newDriveKPP) {
        this.newDriveKPP = newDriveKPP;
    }
    
    public int DecodeDrive(String drive)
    {
        int driveNumber =0;
        switch(drive)
        {
            case "P":
                driveNumber=0;
                break;
             case "R":
                driveNumber=1;
                break;
             case "N":
                driveNumber=2;
                break;
             case "D":
                 driveNumber=3;
                       
        }
        return driveNumber;
           
    }
    
    public void moveForward()
    {
        communicatorKPP.writeData(95);        
    }
    
    public void moveBackward()
    {        
        communicatorKPP.writeData(-95);
    }
  
     public void smallMoveForward()
    {
        communicatorKPP.writeData(90);  
        try {
            Thread.sleep(SleepTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChangeDriveServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      public void stopMove()
    {
        communicatorKPP.writeData(0);  
         try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChangeDriveServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void smallMoveBackward()
    {        
        communicatorKPP.writeData(-90);
        try {
            Thread.sleep(SleepTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(ChangeDriveServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
     public void run()
    {
        while(true){
        if(!canDriveKPP.equals(newDriveKPP))
        {
            if(DecodeDrive(canDriveKPP)<DecodeDrive(newDriveKPP))
            {   
                moveBackward();
                needSmallGo=10;
            }
            else
            {                
                moveForward();
                needSmallGo=-10;
            }           
        }
        else
        {
            if(needSmallGo<0){
                
                smallMoveBackward();
            }
            if(needSmallGo>0)
            {
                smallMoveForward();
            }
            needSmallGo=0;
            stopMove();
                      
        }                    
    }    
    }
}
