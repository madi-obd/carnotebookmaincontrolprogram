/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonsocket;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 *
 * @author Мади
 */
public class XMLTransferUtil {
    
    
    public static String CarDataToString(DataFromCar dfc)
    {
        String xml =null;
            
        XMLEncoder encoder=null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try{
        encoder=new XMLEncoder(new BufferedOutputStream(baos));
        encoder.writeObject(dfc);      
        encoder.close();
         
        xml = baos.toString();
      
        }catch(Exception fileNotFound){
                System.out.println("Error serialize CarData");
        }
        return xml;
    }
       
    public static String AndroidDataToString(DataFromControlAndroid dfc)
    {
        String xml =null;
        
        
        XMLEncoder encoder=null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try{
        encoder=new XMLEncoder(new BufferedOutputStream(baos));
        encoder.writeObject(dfc);
         
        encoder.close();
         
        xml = baos.toString();
       
        }catch(Exception fileNotFound){
                System.out.println("Error serialize CarData");
        }
        return xml;
    }
    
    public static  DataFromControlAndroid StringToAndroidData(String FromAndroidString)
    {        
                InputStream is = new ByteArrayInputStream(FromAndroidString.getBytes());
		BufferedInputStream bis = new BufferedInputStream(is);
		XMLDecoder xmlDecoder = new XMLDecoder(bis);
		DataFromControlAndroid mb = (DataFromControlAndroid) xmlDecoder.readObject();
                return mb;
        
    }
    
    public static  DataFromCar StringToCarData(String FromCarString)
    {        
                InputStream is = new ByteArrayInputStream(FromCarString.getBytes());
		BufferedInputStream bis = new BufferedInputStream(is);
		XMLDecoder xmlDecoder = new XMLDecoder(bis);
		DataFromCar mb = (DataFromCar) xmlDecoder.readObject();
                return mb;       
    }
    
    
}
