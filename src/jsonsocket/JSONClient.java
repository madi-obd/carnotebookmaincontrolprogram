/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonsocket;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Мади
 */
public class JSONClient
{// """
    //A JSON socket server used to communicate with a JSON socket client. All the
    //data is serialized in JSON. How to use it:
    
    String host;
    int port;
    Socket socket = null;   
    List<ClientThread> ClientrThreads  ;
        
    public JSONClient(String host, int port)
    {
        this.host = host;
        this.port = port; 
        ClientrThreads = new ArrayList<ClientThread>();
    }
    
    public void Start()
    {
        try
        {
             
            socket = new Socket(host, port);


            ClientThread st;
            st = new ClientThread( socket );               
            Thread t = new Thread(st);
          
            t.start();            
            ClientrThreads.add(st );                
             
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        } // вывод исключений
                    
    }
     
    
    
    private class ClientThread implements Runnable{
        
        Socket s;
        void Send(OutputStream outStream, DataFromControlAndroid androidData)
        {

            String toCarJSON = JSONTransferUtil.AndroidDataToString(androidData);

            SocketHelperCommunicator._send(outStream, toCarJSON.getBytes());


        }
        
        DataFromCar Receive(BufferedReader inpStream)
        {
            
            String fromCar = SocketHelperCommunicator._recv( inpStream);
            
            return JSONTransferUtil.StringToCarData(fromCar);
            
        }
        
        boolean NeedTerminate;

        public void setNeedTerminate(boolean NeedTerminate) {
            this.NeedTerminate = NeedTerminate;
        }

        public boolean isNeedTerminate() {
            return NeedTerminate;
        }
        public ClientThread(Socket s)
        {            
            this.s=s;
             // и запускаем новый вычислительный поток (см. ф-ю run())                    
        }
        @Override       
        public void run()
        {
            try
            {
                InputStream is = s.getInputStream();
                
                BufferedInputStream br = new BufferedInputStream(is);
                BufferedReader brr = new BufferedReader( new InputStreamReader(br));
                
                while(!isNeedTerminate()){
                   
                // из сокета клиента берём поток входящих данных
                
                // и оттуда же - поток данных от сервера к клиенту
                DataFromCar dfc = Receive(brr);
                
                OutputStream os = s.getOutputStream();
                DataFromControlAndroid dfa = new DataFromControlAndroid();
                dfa.setAcceleratioRate(50);
                dfa.setSteeringWheelRate(50);
                    Send(os, dfa);
                }
                s.close();
                
                System.out.println("Close");
                
            }
            catch(Exception e)
            {                                
                System.out.println("communicate error: "+e);} // вывод исключений
            }
        }
}