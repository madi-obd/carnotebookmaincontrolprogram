/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innertialnavi;

/**
 *
 * @author Admin
 */
public class CarNav {
    
    private double x=0, y=0, angle=0;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public CarNav(double x, double y, double angle) {
        
        this.x=x;
        this.y=y;
        this.angle=angle;
        
                
    }
    
    
}
