/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innertialnavi;

/**
 *
 * @author Admin
 */
public class InnertialNaviServer {
    
    //Car ground speed
    private double speed=0;

    public InnertialNaviServer(float speed, float x, float y, float carAngle, float base_of_car) {
        
        this.speed=speed;
        this.x=x;
        this.y=y;
        this.carAngle=carAngle;
        
        this.base_of_car=base_of_car;
        
    }
    //Car x pos
    private double x=0;
    //Car y pos
    private double y=0;
    
    private double carAngle=0;
    
    private double x_speed=0, y_speed=0, carAngle_Speed=0;
    
    private double base_of_car=3;
    
    public double ValidateAngle(double angle)
    {
        if(angle>360){angle-=360;}
        if(angle<-360){angle+=360;}
        
        return angle;
    }
    public CarNav CalculateNavigation(double wheel_angle, double speed, double delta_time)
    {
        this.wheelAngle=wheel_angle;
        this.speed=speed;
        //Угловая скорость
        carAngle_Speed=speed*Math.tan(wheel_angle)/base_of_car;
        carAngle+=carAngle_Speed*delta_time;
        
        carAngle=ValidateAngle(carAngle);
        
        //Изменение координат
        x_speed=speed*Math.cos(carAngle);
        y_speed=speed*Math.sin(carAngle);
        
        x+=x_speed*delta_time;
        y+=y_speed*delta_time;
        
        return new CarNav(x, y, carAngle);
    }
    

    public double getCarAngle() {
        return carAngle;
    }

    public void setCarAngle(double carAngle) {
        this.carAngle = carAngle;
    }

    public double getX_speed() {
        return x_speed;
    }

    public void setX_speed(double x_speed) {
        this.x_speed = x_speed;
    }

    public double getY_speed() {
        return y_speed;
    }

    public void setY_speed(double y_speed) {
        this.y_speed = y_speed;
    }

    public double getCarAngle_Speed() {
        return carAngle_Speed;
    }

    public void setCarAngle_Speed(double carAngle_Speed) {
        this.carAngle_Speed = carAngle_Speed;
    }
    
    //wheel angle
    private double wheelAngle=0;

    public double getWheelAngle() {
        return wheelAngle;
    }

    public void setWheelAngle(double wheelAngle) {
        this.wheelAngle = wheelAngle;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
  
    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }   
}
